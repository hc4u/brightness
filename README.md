anbrightness
==========

ArtNet display brightness control for OS X.

This tool enables remote programmatic control of OS X screen brightness. You can dim on-stage Apple screen backlights (and thus the Apple logo) remotely from your lighting console.

Install From Source
------------------

```shell
git clone https://bitbucket.org/hc4u/brightness.git
cd brightness
make
sudo make install
```
